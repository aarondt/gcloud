
import pandas as pd
import requests
from tqdm import tqdm
from tqdm import tqdm_notebook
import shutil
import numpy as np
import multiprocessing
from PIL import Image
import cv2
import os

# # Create a function called "chunks" with two arguments, l and n:
# def chunks(l, n):
#     # For item i in a range that is a length of l,
#     for i in range(0, len(l), n):
#         # Create an index range for l of n items:
#         yield l[i:i+n]
files_in_dir = os.listdir("resized/")

def download_data(x):
        for i in tqdm(x):
            if (i+".jpg") in files_in_dir:
                continue
            try:
                target_size = (224,224)
                image = Image.open("bilder/"+i+".jpg")
                if image.mode != "RGB":
                    	image = image.convert("RGB")

                desired_size = target_size[0]
                img = np.array(image)
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                old_size = img.shape[:2] #np.array(img)
                ratio = float(desired_size)/max(old_size)
                new_size = tuple([int(x*ratio) for x in old_size])
                im = cv2.resize(img, (new_size[1], new_size[0]))
                delta_w = desired_size - new_size[1]
                delta_h = desired_size - new_size[0]
                top, bottom = delta_h//2, delta_h-(delta_h//2)
                left, right = delta_w//2, delta_w-(delta_w//2)

                color = [255, 255, 255] # white padding_that_image
                image = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,value=color)
                cv2.imwrite("resized/"+i+".jpg", image)
            except:
                print("error")
            # with open("C:/Users/aaron/Desktop/Freisteller_resized/"+i+".jpg", 'wb') as f:
            #     f.write(image)

# def download_data2(x):
#         for i in tqdm(x):
#             response = requests.get("http://cdn.aboutstatic.com/file/"+i+"?quality=90&progressive=1&trim=1&bg=f2f2f2&width=500&height=500")
#             with open("C:/Users/aaron/Desktop/DATA/"+i+".jpg", 'wb') as f:
#                 f.write(response.content)
#
# def download_data3(x):
#         for i in tqdm(x):
#             response = requests.get("http://cdn.aboutstatic.com/file/"+i+"?quality=90&progressive=1&trim=1&bg=f2f2f2&width=500&height=500")
#             with open("C:/Users/aaron/Desktop/DATA/"+i+".jpg", 'wb') as f:
#                 f.write(response.content)
#
# def download_data4(x):
#         for i in tqdm(x):
#             response = requests.get("http://cdn.aboutstatic.com/file/"+i+"?quality=90&progressive=1&trim=1&bg=f2f2f2&width=500&height=500")
#             with open("C:/Users/aaron/Desktop/DATA/"+i+".jpg", 'wb') as f:
#                 f.write(response.content)
#
# def download_data5(x):
#         for i in tqdm(x):
#             response = requests.get("http://cdn.aboutstatic.com/file/"+i+"?quality=90&progressive=1&trim=1&bg=f2f2f2&width=500&height=500")
#             with open("C:/Users/aaron/Desktop/DATA/"+i+".jpg", 'wb') as f:
#                 f.write(response.content)


if __name__ == "__main__":
    from multiprocessing import Pool
    #df = pd.read_csv("C:/Users/aaron/Desktop/Analyse/Fashion_Frauen_komplett/women_bekleidung_all.csv")
    #df = df[(df.type == "bust") & (df.view == "front")]
    #urls = df.url.apply(lambda x: "http://cdn.aboutstatic.com/file/"+str(x)+"?quality=90&progressive=1&trim=1&bg=f2f2f2&width=800&height=800")
    #file_names = df.url#.apply(lambda x: x+".jpg")
    #file_names = df.url#.apply(lambda x: x+".jpg")
    files_in_dir = os.listdir("bilder/")
    data = [x.split(".jpg")[0] for x in files_in_dir]#file_names.values
    #print(data)

    a,b,c,d,e = np.array_split(data,5)

    #a,b,c = chunks(data,3)
    # creating new processes
    p1 = multiprocessing.Process(target=download_data, args=(a,))
    p2 = multiprocessing.Process(target=download_data, args=(b,))
    p3 = multiprocessing.Process(target=download_data, args=(c,))
    p4 = multiprocessing.Process(target=download_data, args=(d,))
    p5 = multiprocessing.Process(target=download_data, args=(e,))


    # starting processes
    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()

    # wait until processes are finished
    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()

    # print final balance
    print("Finish")

    # l = data # list of all your image files
    #
    # f = download_data# function to modify each of these, taking element of l as input.
    #
    # p = Pool(7) # however many process you want to spawn
    #
    # p.map(f, l)
