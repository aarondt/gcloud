
import pandas as pd
import requests
from tqdm import tqdm
from tqdm import tqdm_notebook
import shutil
import numpy as np
import multiprocessing
import os

# # Create a function called "chunks" with two arguments, l and n:
# def chunks(l, n):
#     # For item i in a range that is a length of l,
#     for i in range(0, len(l), n):
#         # Create an index range for l of n items:
#         yield l[i:i+n]



def download_data(x):
        for i in tqdm(x):
            if (i+".jpg") in files_in_dir:
                continue
            # try:
            response = requests.get("http://cdn.aboutstatic.com/file/"+i+"?quality=90&progressive=1&trim=1&bg=f2f2f2&width=500&height=500")
            with open("bilder/"+i+".jpg", 'wb') as f:
                f.write(response.content)
            # except:
            #     print("error")




if __name__ == "__main__":
    from multiprocessing import Pool


    df_images = pd.read_csv("product_images_sample.csv",sep=";")
    df = pd.read_csv("product_categories_sample.csv",sep=";")
    df_attributes = pd.read_csv("product_attributes_sample.csv",sep=";")

    # df_categories aufbereiten damit jede Hierarchie einem Level zugeordnet ist
    category_path = list(df.category_path.str.split("|"))
    max(category_path,key=len)
    col_names = list()
    for i in [x for x in range(1,9)]:
        col_names.append("lvl"+str(i))
    levels_df = pd.DataFrame(category_path, columns=col_names)
    df_categories = pd.concat([df, levels_df],axis=1)
    df_categories.drop("category_path",axis=1,inplace=True)

    # Verbinde Bild- und Attribut-Daten: Nun mehrere Rows pro ID, da verschiedene Attribute
    df_merged = pd.merge(df_images,df_categories,how="left", on="product_id")

    df_merged = df_merged[(df_merged.lvl2 == "Fashion")& (df_merged.type == "model") & (~df_merged.lvl4.isin(["Schuhe", "Accessoires"])) & (df_merged.focus != "detail") ]

    df_merged = df_merged.drop_duplicates(subset=['product_id', 'type', 'focus', 'view', 'angle',
       'category_id', 'lvl1', 'lvl2', 'lvl3', 'lvl4', 'lvl5', 'lvl6', 'lvl7',
       'lvl8'])

    file_names = df_merged.url#.apply(lambda x: x+".jpg")
    data = file_names.values

    files_in_dir = os.listdir("bilder/")
    data = [x for x in data if x+".jpg"  not in files_in_dir ]
    print("Erwarte {} Files im Dir".format(len(data)))
    a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p = np.array_split(data,16)

    #a,b,c = chunks(data,3)
    # creating new processes
    p1 = multiprocessing.Process(target=download_data, args=(a,))
    p2 = multiprocessing.Process(target=download_data, args=(b,))
    p3 = multiprocessing.Process(target=download_data, args=(c,))
    p4 = multiprocessing.Process(target=download_data, args=(d,))
    p5 = multiprocessing.Process(target=download_data, args=(e,))
    p6 = multiprocessing.Process(target=download_data, args=(f,))
    p7 = multiprocessing.Process(target=download_data, args=(g,))
    p8 = multiprocessing.Process(target=download_data, args=(h,))
    p9 = multiprocessing.Process(target=download_data, args=(i,))
    p10 = multiprocessing.Process(target=download_data, args=(j,))
    p11 = multiprocessing.Process(target=download_data, args=(k,))
    p12 = multiprocessing.Process(target=download_data, args=(l))
    p13 = multiprocessing.Process(target=download_data, args=(m,))
    p14 = multiprocessing.Process(target=download_data, args=(n,))
    p15 = multiprocessing.Process(target=download_data, args=(o,))
    p16 = multiprocessing.Process(target=download_data, args=(p,))




    # starting processes
    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()
    p6.start()
    p7.start()
    p8.start()

    p9.start()
    p10.start()
    p11.start()
    p12.start()
    p13.start()
    p14.start()
    p15.start()
    p16.start()


    # wait until processes are finished
    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    p7.join()
    p8.join()

    p9.join()
    p10.join()
    p11.join()
    p12.join()
    p13.join()
    p14.join()
    p15.join()
    p16.join()



    # print final balance
    print("Finish")

    # l = data # list of all your image files
    #
    # f = download_data# function to modify each of these, taking element of l as input.
    #
    # p = Pool(7) # however many process you want to spawn
    #
    # p.map(f, l)
