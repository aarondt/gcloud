import pandas as pd

########################
cat = "maternity"

# <---- Suche hiernach!
########################


##### Keras reset
from keras.backend.tensorflow_backend import set_session
from keras.backend.tensorflow_backend import clear_session
from keras.backend.tensorflow_backend import get_session
import tensorflow
import gc

# Reset Keras Session
def reset_keras():
    sess = get_session()
    clear_session()
    sess.close()
    sess = get_session()

    try:
        del classifier # this is from global space - change this as you need
    except:
        pass

    print(gc.collect()) # if it's done something you should see a number being outputted

    # use the same config as you used to create the session
    config = tensorflow.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 1
    config.gpu_options.visible_device_list = "0"
    set_session(tensorflow.Session(config=config))

reset_keras()

csv_dir = "C:/Users/aaron/Desktop/Analyse/Email-Daten/"

df_images = pd.read_csv("product_images_sample.csv",sep=";")
df = pd.read_csv("product_categories_sample.csv",sep=";")
df_attributes = pd.read_csv("product_attributes_sample.csv",sep=";")
df_categories = pd.read_csv("product_categories_bearbeitet.csv",sep=",")

df_images =  pd.read_csv("master_train.csv")



# Es werden nachfolgend nur Bilder berücksichtigt die: type=bust, view=front, focus=product
df = df_images[(df_images.type == "bust") & (df_images.focus == "product") & (df_images.view=="front")]

# ich möchte von jeden Produkt nur jeweils EINE FRONT Ansicht im Datensatz enthalten haben
df = df.drop_duplicates(subset="product_id")
print(len(df))

# Verbinde Bild- und Attribut-Daten: Nun mehrere Rows pro ID, da verschiedene Attribute
df_merged = pd.merge(df,df_attributes,how="left", on="product_id")

# Filter nach KATEGORIE, da alle Pflichtattribute ausschließlich in KATEGORIE sind
df_merged = df_merged[df_merged.attribute_group_group == "Kategorie"]

df_categories = df_categories.drop_duplicates(subset="product_id")
len(df_categories.drop_duplicates(subset="product_id"))

df_merged = pd.merge(df_merged,df_categories,how="left",on="product_id")

# ########################
# cat = "coat_style"   # <---- Suche hiernach!
# ########################

# produkt_texte unbearbeitete original Quelle
produkt_texte = pd.read_csv("produkt_texte.csv")
produkt_texte.head()

df_merged = df_merged[df_merged["attribute_group_name"]==cat]

df_merged = pd.merge(df_merged,produkt_texte,how="left",on="product_id")

#result = df_merged.pivot_table(index=["product_id","url","text"],columns=["attribute_group_name","attribute_name"],aggfunc="count")["attribute_group_group"]
result = df_merged.pivot_table(index=["product_id","url"],columns=["attribute_group_name","attribute_name"],aggfunc="count")["attribute_group_group"]
result = result.fillna(value=0)
result = result.reset_index()

#pd.set_option('display.max_colwidth', -1)

# Entferne Multiindex
result.columns = ["product_id","url"]+list(result.columns.droplevel(0)[2:])
# Füge Text hinzu
result = pd.merge(result,produkt_texte, how="left",on="product_id")

# Welcher Anteil im Datensatz verfügt über Text?

print("Anteil mit Text:",len(result[result['text'].str.len() > 3])/ len(result.text))

# Wandle Columns in eine Column um, sodass es für flow_from_dataframe aufbereitet ist
def get_categorical(row):
    for c in result.columns[2:-2]:
        if row[c]==1:
            return c

labels = pd.DataFrame(result.apply(get_categorical, axis=1))
labels.rename(columns={0:"labels"},inplace=True)
result["labels"] = labels.labels
df_predict = result.copy()

df_predict = pd.merge(df_predict,df_categories,how="left",on="product_id")

import os
bilder_in_dir = os.listdir("resized/")
vorher = len(df_predict)

df_predict["filename"] = df_predict.url.apply(lambda x: x + ".jpg")
df_predict = df_predict[df_predict.filename.isin(bilder_in_dir)]
nachher = len(df_predict)
print("**** Es fehlen {} Bilder".format(str(vorher-nachher)))
#df_predict.head(3)

import keras
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Dense, Activation, Flatten, Dropout, BatchNormalization
from keras.layers import Conv2D, MaxPooling2D
from keras import regularizers, optimizers
import pandas as pd
import numpy as np
#from keras_tqdm import TQDMNotebookCallback
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from keras.models import Model
from sklearn.model_selection import train_test_split
# Teile Dataframe für 10% Test-Set
total_data = df_predict[["product_id","filename","labels","url"]].copy()

data, test = train_test_split(total_data, test_size=0.10, stratify=total_data.labels, random_state=32)
data = data.reset_index()
test = test.reset_index()

#datagen=ImageDataGenerator(rescale=1./255,validation_split=0.15)
# def prep_data(x):
#     x = tf.image.rgb_to_grayscale(x)
#     #x = keras.applications.mobilenet.preprocess_input(x)
#     return x

bild_dir = "resized/"

datagen=ImageDataGenerator(preprocessing_function=keras.applications.mobilenet.preprocess_input,validation_split=0.10)
training_on  = "url"
target = "labels"
batch_size = 32
data_dir = bild_dir


train_generator=datagen.flow_from_dataframe(dataframe=data, directory=data_dir, x_col=training_on, y_col=target, has_ext=False, class_mode="categorical", target_size=(224,224), batch_size=batch_size,subset="training",seed = 1)
validation_generator=datagen.flow_from_dataframe(dataframe=data, directory=data_dir, x_col=training_on, y_col=target, has_ext=False, class_mode="categorical", target_size=(224,224), batch_size=batch_size,subset="validation",shuffle=True,seed = 1)

# Test-Set definieren
testgen=ImageDataGenerator(preprocessing_function=keras.applications.mobilenet.preprocess_input)
training_on  = "url"
target = "labels"
batch_size = 1
data_dir = bild_dir


#test_generator = testgen.flow_from_dataframe(dataframe=test, directory=data_dir, x_col=training_on, y_col=None, has_ext=True, class_mode=None, target_size=(224,224), batch_size=batch_size,shuffle=False,seed = 1)
test_generator = testgen.flow_from_dataframe(classes=list(train_generator.class_indices.keys()),dataframe=test, directory=data_dir, x_col=training_on, y_col=target, has_ext=False, class_mode="categorical", target_size=(224,224), batch_size=batch_size,shuffle=False,seed = 1)

# save class mapping

np.save("Modelle_freisteller/{}_classmapping.npy".format(cat), list(train_generator.class_indices.keys()))

# weighting
from sklearn.utils import class_weight
class_weights = class_weight.compute_class_weight('balanced',list(pd.Series(train_generator.classes).unique()),train_generator.classes)
class_weight_dict = dict(enumerate(class_weights))
class_weight_dict

mobile = keras.applications.mobilenet.MobileNet(include_top = False, input_shape = (224, 224, 3))

#x = mobile.layers[-6].output
x = mobile.output
x = Flatten()(x)
x = Dropout(0.5)(x)
x = Dense(30, activation='relu')(x)

x = Dropout(0.5)(x)
#x = Dense(16, activation='relu')(x)
predictions = Dense(len(train_generator.class_indices), activation="softmax")(x)
model = Model(inputs=mobile.input, outputs=predictions)

for layer in mobile.layers:
    layer.trainable = False

model.summary()


model.compile(optimizer=optimizers.Adam(lr=2e-5),loss="categorical_crossentropy", metrics=['acc'])
STEPS_EPOCH = train_generator.n//train_generator.batch_size
VALID_STEPS = validation_generator.n//validation_generator.batch_size

#89/89 [==============================] - 6s 66ms/step - loss: 0.5645 - acc: 0.7555 - val_loss: 0.7207 - val_acc: 0.7041

from keras.callbacks import ModelCheckpoint
from keras.callbacks import CSVLogger

reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                              patience=3, min_lr=0.00001)

csv_logger = CSVLogger('log_{}.csv'.format(cat), append=True, separator=',')



checkpointer = ModelCheckpoint(filepath='Modelle_freisteller/{}_model.h5'.format(cat), verbose=1, save_best_only=True)
early = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=5, verbose=1, mode='auto', baseline=None, restore_best_weights=True)
from keras.callbacks import CSVLogger

csv_logger = CSVLogger('Logs/log_{}.csv'.format(cat), append=True, separator=',')

history = model.fit_generator(train_generator,steps_per_epoch=STEPS_EPOCH,validation_steps=VALID_STEPS ,
                    validation_data=validation_generator, epochs=30, verbose=1, callbacks= [checkpointer,early,reduce_lr,csv_logger])#, callbacks=[csv_logger])#,callbacks=[TQDMNotebookCallback()])

for count,layer in enumerate(model.layers):
    if "_13" in layer.name:
        print(count)
        print(layer.name)

for count,layer in enumerate(model.layers):
    layer.trainable = False

for count,layer in enumerate(model.layers[81:]):
    layer.trainable = True




model.compile(optimizer=optimizers.Adam(lr=1e-5),loss="categorical_crossentropy", metrics=['acc'])
history = model.fit_generator(train_generator,steps_per_epoch=STEPS_EPOCH,validation_steps=VALID_STEPS ,
                    validation_data=validation_generator, epochs=5, verbose=1,class_weight=class_weight_dict, callbacks=[checkpointer,early,reduce_lr])#, callbacks=[csv_logger])#,callbacks=[TQDMNotebookCallback()])
